﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prog_milestone3_10006291
{
    public class Pizza
    {
        public string size1 = "Small";
        public string size2 = "Medium";
        public string size3 = "Large";

        public string type1 = "Hawaiian";
        public string type2 = "Ham and Cheese";
        public string type3 = "Meat Lovers";
        public string type4 = "Cheese Lovers";
        public string type5 = "Veggie Delight";
    }
    class Program
    {
        static void greeting()
        {
            string name;
            string phonenumber;
            Console.WriteLine("Welcome to the Pizza Shop. Before you place your order please enter your name and phone number.");
            name = Console.ReadLine();
            phonenumber = Console.ReadLine();
        }

        
        static void Menu()
        {           
            Console.WriteLine("\n           Pizza    Menu");
            Console.WriteLine("****************************************\n");
            Console.WriteLine("1. Hawaiian\n");
            Console.WriteLine("2. Ham and Cheese\n");
            Console.WriteLine("3. Meat Lovers\n");
            Console.WriteLine("4. Cheese Lovers\n");
            Console.WriteLine("5. Veggie Delight\n");
            Console.WriteLine("****************************************\n");
            Console.WriteLine("         Please select an option");
        }
        static void Main(string[] args)            
        {
            string name;
            string phonenumber;                        
            Console.WriteLine("Welcome to Pizza Palace. Before you place your order please enter your name and phone number.");
            name = Console.ReadLine();
            phonenumber = Console.ReadLine();
                                    
            Console.WriteLine("Thank you. Please place your order from the menu.");

            Console.WriteLine("\n           Pizza    Menu");
            Console.WriteLine("****************************************\n");
            Console.WriteLine("1. Hawaiian\n");
            Console.WriteLine("2. Ham and Cheese\n");
            Console.WriteLine("3. Meat Lovers\n");
            Console.WriteLine("4. Cheese Lovers\n");
            Console.WriteLine("5. Veggie Delight\n");
            Console.WriteLine("****************************************\n");
            Console.WriteLine("         Please select an option");

            var pizzaselection = Console.ReadLine();
            switch (pizzaselection)
            {
                case "1":
                    Console.WriteLine("Hawaiian\n");
                    break;
                case "2":
                    Console.WriteLine("Ham and Cheese\n");
                    break;
                case "3":
                    Console.WriteLine("Meat Lovers\n");
                    break;
                case "4":
                    Console.WriteLine("Cheese Lovers\n");
                    break;
                case "5":
                    Console.WriteLine("Veggie Delight\n");
                    break;
                default:
                    Console.WriteLine("The selection was invalid\n");
                    break;
            }

            Console.WriteLine("Please also select a size for your pizza");

            Console.WriteLine("\n          Pizza     Sizes");
            Console.WriteLine("***************************************\n");
            Console.WriteLine("1. Small $5.00\n");
            Console.WriteLine("2. Medium $7.00\n");
            Console.WriteLine("3. Large $9.00\n");
            Console.WriteLine("***************************************\n");
            Console.WriteLine("        Please select an option");

            var pizzasizeselection = Console.ReadLine();
            switch (pizzasizeselection)
            {
                case "1":
                    Console.WriteLine("Small\n");
                    break;
                case "2":
                    Console.WriteLine("Medium\n");
                    break;
                case "3":
                    Console.WriteLine("Large\n");
                    break;
                default:
                    Console.WriteLine("The selection was invalid\n");
                    break;
            }

            Console.WriteLine("Please also choose a drink if you would like");

            Console.WriteLine("\n            Drinks    Menu");
            Console.WriteLine("*******************************************\n");
            Console.WriteLine("1. Coke $5.00\n");
            Console.WriteLine("2. Sprite $4.50\n");
            Console.WriteLine("3. Orange Juice $6.50\n");
            Console.WriteLine("4. Fanta $5.50\n");
            Console.WriteLine("5. Water $4.00\n");
            Console.WriteLine("*******************************************\n");
            Console.WriteLine("         Please select an option");

            var drinksmenu = Console.ReadLine();
            switch (drinksmenu)
            {
                case "1":
                    Console.WriteLine("Coke\n");
                    break;
                case "2":
                    Console.WriteLine("Sprite\n");
                    break;
                case "3":
                    Console.WriteLine("Orange Juice\n");
                    break;
                case "4":
                    Console.WriteLine("Fanta\n");
                    break;
                case "5":
                    Console.WriteLine("Water\n");
                    break;
                default:
                    Console.WriteLine("The selection was invalid\n");
                    break;
            }
            var pizzasize = new List<Tuple<string, double>>();
            pizzasize.Add(Tuple.Create("Small", 5.00));
            pizzasize.Add(Tuple.Create("Medium", 7.00));
            pizzasize.Add(Tuple.Create("Large", 9.00));
                     
            foreach (var x in pizzasize)
            {
                Console.WriteLine($"The pizza size you ordered is {x}");
            }

            var pizzatype = new List<Tuple<string>>();
            pizzatype.Add(Tuple.Create("Hawaiian"));
            pizzatype.Add(Tuple.Create("Ham and Cheese"));
            pizzatype.Add(Tuple.Create("Meat Lovers"));
            pizzatype.Add(Tuple.Create("Cheese Lovers"));
            pizzatype.Add(Tuple.Create("Veggie Delight"));

            foreach (var x in pizzatype)
            {
                Console.WriteLine($"The type of pizza you ordered is {x}");
            }

            var drinks = new List<Tuple<string, double>>();
            drinks.Add(Tuple.Create("Coke", 5.00));
            drinks.Add(Tuple.Create("Sprite", 4.50));
            drinks.Add(Tuple.Create("Orange Juice", 6.50));
            drinks.Add(Tuple.Create("Fanta", 5.50));    
            drinks.Add(Tuple.Create("Water", 4.00));
                     

                foreach (var x in drinks)
            {
                Console.WriteLine($"The drink you ordered is {x}");
            }

           Console.ReadLine();

                }
            }
        }

    


